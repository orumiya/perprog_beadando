﻿using AerialPhotoColoringWPF.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerialPhotoColoringWPF.ViewModel
{
    public class FilterViewModel
    {
        public List<FilterType> usedFilters;
        public FilterViewModel()
        {
            this.usedFilters = new List<FilterType>();
        }

        public bool? isBrightnessUsed { get; set; }
        public int Brightness { get; set; }

        public bool? isContrastUsed { get; set; }
        public int Contrast { get; set; }

        public bool? isEdgeDetectionUsed { get; set; }
        public bool? isSharpenUsed { get; set; }

        public bool? isVARIUsed { get; set; }
        public double VARIRangeStart { get; set; }
        public double VARIRangeEnd { get; set; }


        public void addUsedFilters()
        {
            if (isBrightnessUsed == true)
            {
                usedFilters.Add(FilterType.BRIGHTNESS);
            }
            if (isContrastUsed == true)
            {
                usedFilters.Add(FilterType.CONTRAST);
            }
            if (isEdgeDetectionUsed == true)
            {
                usedFilters.Add(FilterType.EDGEDETECTION);
            }
            if (isSharpenUsed == true)
            {
                usedFilters.Add(FilterType.SHARPEN);
            }
            if (isVARIUsed == true)
            {
                usedFilters.Add(FilterType.VARI);
            }
        }
    }
}
