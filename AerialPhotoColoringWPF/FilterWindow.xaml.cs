﻿using AerialPhotoColoringWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AerialPhotoColoringWPF
{
    /// <summary>
    /// Interaction logic for FilterWindow.xaml
    /// </summary>
    public partial class FilterWindow : Window
    {
        FilterViewModel filterViewModel;
        int brightnessValue = 0;
        int contrastValue = 0;
        double variRangeStart = -1;
        double variRangeEnd = 1;

        public FilterWindow(FilterViewModel vm)
        {
            filterViewModel = vm;
            InitializeComponent();
        }

        private void SaveFilterButton_Click(object sender, RoutedEventArgs e)
        {
            filterViewModel.isBrightnessUsed = brightness_checkbox.IsChecked;
            filterViewModel.Brightness = brightnessValue;
            filterViewModel.isContrastUsed = contrast_checkbox.IsChecked;
            filterViewModel.Contrast = contrastValue;
            filterViewModel.isSharpenUsed = sharpen_radio.IsChecked;
            filterViewModel.isEdgeDetectionUsed = edge_radio.IsChecked;
            filterViewModel.isVARIUsed = vari_radio.IsChecked;
            filterViewModel.VARIRangeStart = variRangeStart;
            filterViewModel.VARIRangeEnd = variRangeEnd;
            filterViewModel.addUsedFilters();

            this.Close();
        }

        private void Brightness_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            brightnessValue = Convert.ToInt32(e.NewValue);
            string msg = String.Format("{0}", brightnessValue);
            this.brightness_label.Content = msg;
        }

        private void Contrast_slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            contrastValue = Convert.ToInt32(e.NewValue);
            string msg = String.Format("{0}", contrastValue);
            this.contrast_label.Content = msg;

        }

        private void Vari_slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            variRangeStart = Convert.ToDouble(vari_slider1.Value);
            variRangeEnd = Convert.ToDouble(vari_slider2.Value);
            string msg1 = String.Format("{0:0.00}", variRangeStart);
            string msg2 = String.Format("{0:0.00}", variRangeEnd);
            this.vari_label_start.Content = msg1;
            this.vari_label_end.Content = msg2;
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            vari_slider1.Value = -1;
            vari_slider2.Value = 1;
        }
    }
}
