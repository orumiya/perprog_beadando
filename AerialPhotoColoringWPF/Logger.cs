﻿using AerialPhotoColoringWPF.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerialPhotoColoringWPF
{
    public static class Logger
    {
        public static DateTime startTime;
        public static DateTime endTime;
        private static TimeSpan duration;
        public static int processedImages;
        public static List<FilterType> filterTypes;
        public static string paralellType;

        public static void Log(string message)
        {
                using (StreamWriter streamWriter = new StreamWriter("log.txt", append:true))
                {
                    streamWriter.WriteLine(message);
                    streamWriter.Close();
                }
            
        }

        public static TimeSpan Duration(DateTime startTime, DateTime endTime)
        {
            duration = endTime.Subtract(startTime);
            return duration;
        }

        public static string CreateLogMessage(int processedImages, List<FilterType> filterTypes, string paralellType, DateTime startTime, DateTime endTime)
        {
            duration = Duration(startTime, endTime);
            string filters = "";
            foreach (var item in filterTypes)
            {
                filters += item.ToString() + ", ";
            }
            return "Processed images:" + processedImages + ", filter types: " + filters + "paralell type: " + paralellType + ", start time:"
                + startTime.ToLocalTime() + ", end time: " + endTime.ToLocalTime() + ", duration: " + duration;
        }
    }
}
