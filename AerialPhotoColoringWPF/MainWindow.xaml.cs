﻿using AerialPhotoColoringWPF.BL;
using AerialPhotoColoringWPF.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AerialPhotoColoringWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<BitmapImage> selectedImage;
        List<Bitmap> selectedBitmapImage;
        FilterViewModel filterViewModel;
        

        public MainWindow()
        {
            InitializeComponent();
            this.selectedImage = new List<BitmapImage>();
            this.selectedBitmapImage = new List<Bitmap>();
            contrast_main_label.Content = "Contrast";
            brightn_main_label.Content = "Brightness";
            edge_main_label.Content = "Edge detection";
            sharpen_main_label.Content = "Sharpen";
            vari_main_label.Content = "VARI";
            processImageButton.IsEnabled = false;
        }

        private void AddImageButton_Click(object sender, RoutedEventArgs e)
        {
            LoadImagesFromFileDialog();
        }

        private async void ProcessImageButton_Click(object sender, RoutedEventArgs e)
        {
            if (isSequential_radio.IsChecked == true)
            {
                // szekvenciális esetben is szükség van erre, hogy a futó folyamat a fő szálat ne blokkolja (amin a GUI fut)
                await Task.Run(() => SequentialProcess());
                MessageBox.Show("Ready!", "Image processing has finished", MessageBoxButton.OK, MessageBoxImage.Information);
            } else if (isWithTask_radio.IsChecked == true)
            {
                await Task.Run(() => ParalellWithTask());
                MessageBox.Show("Ready!", "Image processing has finished", MessageBoxButton.OK, MessageBoxImage.Information);
            } else if (isWithThread_radio.IsChecked == true)
            {
                Thread t = new Thread(new ThreadStart(() => ParalellWithThread()));
                t.Start();
                t.Join();
                MessageBox.Show("Ready!", "Image processing has finished", MessageBoxButton.OK, MessageBoxImage.Information);
            }
           
        }

        private void AddFilterButton_Click(object sender, RoutedEventArgs e)
        {
            filterViewModel = new FilterViewModel();
            filterViewModel.usedFilters.Clear();
            Window filterWindow = new FilterWindow(filterViewModel);
            filterWindow.ShowDialog();
        }

        private void ShowUsedFilters()
        {
            if (filterViewModel != null)
            {
                if (filterViewModel.usedFilters.Count > 0)
                {
                    processImageButton.IsEnabled = true;
                }
                if (filterViewModel.isContrastUsed == true)
                {
                    contrast_main_label.Content = "Contrast ✔";
                    contrast_main_label.Foreground = System.Windows.Media.Brushes.White;
                    contrast_main_label.Background = System.Windows.Media.Brushes.OrangeRed;
                } else
                {
                    contrast_main_label.Content = "Contrast";
                    contrast_main_label.Foreground = System.Windows.Media.Brushes.Black;
                    contrast_main_label.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2e88b3"));
                }
                if (filterViewModel.isBrightnessUsed == true)
                {
                    brightn_main_label.Content = "Brightness ✔";
                    brightn_main_label.Foreground = System.Windows.Media.Brushes.White;
                    brightn_main_label.Background = System.Windows.Media.Brushes.OrangeRed;
                } else
                {
                    brightn_main_label.Content = "Brightness";
                    brightn_main_label.Foreground = System.Windows.Media.Brushes.Black;
                    brightn_main_label.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2e88b3"));
                }
                if (filterViewModel.isEdgeDetectionUsed == true)
                {
                    edge_main_label.Content = "Edge detection ✔";
                    edge_main_label.Foreground = System.Windows.Media.Brushes.White;
                    edge_main_label.Background = System.Windows.Media.Brushes.OrangeRed;

                } else if (filterViewModel.isSharpenUsed == true)
                {
                    sharpen_main_label.Content = "Sharpen ✔";
                    sharpen_main_label.Foreground = System.Windows.Media.Brushes.White;
                    sharpen_main_label.Background = System.Windows.Media.Brushes.OrangeRed;

                } else if (filterViewModel.isVARIUsed == true)
                // VARI used
                {
                    vari_main_label.Content = "VARI ✔";
                    vari_main_label.Foreground = System.Windows.Media.Brushes.White;
                    vari_main_label.Background = System.Windows.Media.Brushes.OrangeRed;
                } else
                {
                    edge_main_label.Content = "Edge detection";
                    edge_main_label.Foreground = System.Windows.Media.Brushes.Black;
                    edge_main_label.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2e88b3"));
                    sharpen_main_label.Content = "Sharpen";
                    sharpen_main_label.Foreground = System.Windows.Media.Brushes.Black;
                    sharpen_main_label.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2e88b3"));
                    vari_main_label.Content = "VARI";
                    vari_main_label.Foreground = System.Windows.Media.Brushes.Black;
                    vari_main_label.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2e88b3"));
                }

            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            ShowUsedFilters();
        }

        private void SequentialProcess()
        {
            DateTime startTime = DateTime.Now;
            for (int i = 0; i < selectedBitmapImage.Count; i++)
            {
                string currentFileName = "";
                for (int j = 0; j < filterViewModel.usedFilters.Count; j++)
                {
                    if (j == 0)
                    {
                        currentFileName = ImageHandler.ProcessImageData(selectedBitmapImage[i], i, filterViewModel, filterViewModel.usedFilters[j]);
                    }
                    else
                    {
                        currentFileName = ImageHandler.ProcessImageData(new Bitmap(currentFileName), i, filterViewModel, filterViewModel.usedFilters[j]);
                    }
                }
            }
            DateTime endTime = DateTime.Now;

            string logMessage = Logger.CreateLogMessage(selectedBitmapImage.Count, filterViewModel.usedFilters, "sequential", startTime, endTime);
            Logger.Log(logMessage);
        }

        private void ParalellWithTask()
        {
            DateTime startTime = DateTime.Now;
            List<Task> taskListPerImages = new List<Task>();

            for (int i = 0; i < selectedBitmapImage.Count; i++)
            {
                Bitmap lambdaBitmap = selectedBitmapImage[i];
                int index = i;
                taskListPerImages.Add(Task.Run(() => ProcessImageDataParalell(lambdaBitmap, index)));
            }
            Task.WaitAll(taskListPerImages.ToArray());
            DateTime endTime = DateTime.Now;

            string logMessage = Logger.CreateLogMessage(selectedBitmapImage.Count, filterViewModel.usedFilters, "paralell with Task", startTime, endTime);
            Logger.Log(logMessage);
        }

        private void ProcessImageDataParalell(Bitmap selectedImage, int index)
        {
            string currentFileName = "";
            for (int j = 0; j < filterViewModel.usedFilters.Count; j++)
            {
                if (j == 0)
                {
                    currentFileName = ImageHandler.ProcessImageData(selectedImage, index, filterViewModel, filterViewModel.usedFilters[j]);
                }
                else
                {
                    currentFileName = ImageHandler.ProcessImageData(new Bitmap(currentFileName), index, filterViewModel, filterViewModel.usedFilters[j]);
                }
            }
        }

        private void LoadImagesFromFileDialog()
        {
            this.selectedBitmapImage.Clear();
            this.selectedImage.Clear();
            // a képek feltöltéséhez egy file feltöltőre van szükség
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Add images";
            fileDialog.Multiselect = true;
            if (fileDialog.ShowDialog() == true)
            {
                for (int i = 0; i < fileDialog.FileNames.Length; i++)
                {
                    int index = i;
                    string currentFileName = fileDialog.FileNames[i];
                    Task.Run(() => {
                        BitmapImage img = new BitmapImage(new Uri(currentFileName));
                        img.CacheOption = BitmapCacheOption.OnLoad;
                        img.DecodePixelHeight = 100;
                        this.selectedImage.Add(img);

                        Bitmap bitmapimage = new Bitmap(currentFileName);
                        selectedBitmapImage.Add(bitmapimage);
                        img.Freeze();

                        Dispatcher.Invoke(new Action(() => {
                            System.Windows.Controls.Image uploadedImage = new System.Windows.Controls.Image();
                            uploadedImage.Source = img;
                            uploadedImage.Stretch = Stretch.UniformToFill;
                            imageGrid.Children.Add(uploadedImage);
                            Grid.SetColumn(uploadedImage, index % 6);
                            Grid.SetRow(uploadedImage, (index / 6) + 1);
                        }));
                        
                    });
                    
                    
                }
            }
        }

        private void ParalellWithThread()
        {
            DateTime startTime = DateTime.Now;
            List<Thread> threadListPerImages = new List<Thread>();

            for (int i = 0; i < selectedBitmapImage.Count; i++)
            {
                Bitmap lambdaBitmap = selectedBitmapImage[i];
                int index = i;
                Thread thread = new Thread(new ThreadStart(() => ProcessImageDataParalell(lambdaBitmap, index)));
                threadListPerImages.Add(thread);
                thread.Start();
            }

            // összevárjuk a Threadeket
            foreach (var thread in threadListPerImages)
            {
                thread.Join();
            }
            DateTime endTime = DateTime.Now;
            string logMessage = Logger.CreateLogMessage(selectedBitmapImage.Count, filterViewModel.usedFilters, "paralell with Thread", startTime, endTime);
            Logger.Log(logMessage);
        }
    }
}
