﻿namespace AerialPhotoColoringWPF.BL
{
    using System;
    using System.Drawing;

    public static class VARIFilter
    {
        public static float Convert(Color color)
        {
            //Console.Write(color.G + "  " + color.B + " " + color.R);
            float result = ((float)(color.G - color.R) / (float)(color.R + color.G - color.B));
            return result;

        }

        public static double[] SetScaleValues(double[] defaultScaleValues, double minValue, double maxValue)
        {
            double defaultLength = 2;
            double newScaleLength = maxValue - minValue;
            double[] scaleValues = new double[defaultScaleValues.Length];
            double lastValue = minValue;
            for (int i = 0; i < defaultScaleValues.Length; i++)
            {
                double ratio = Math.Abs((defaultScaleValues[i] / defaultLength) * newScaleLength);
                scaleValues[i] = ratio + lastValue;
                lastValue = scaleValues[i];
            }
            return scaleValues;
        }

        // scaleValues contains 9 values to scale the VARI coloring
        public static Color ToColorMapper(float VARIvalue, double[] scaleValues)
        {
            if (VARIvalue >= scaleValues[8])
            {
                return Color.DarkGreen;
            }
            else if (VARIvalue >= scaleValues[7] && VARIvalue < scaleValues[8])
            {
                return Color.Green;
            }
            else if (VARIvalue >= scaleValues[6] && VARIvalue < scaleValues[7])
            {
                return Color.LimeGreen;
            }
            else if (VARIvalue >= scaleValues[5] && VARIvalue < scaleValues[6])
            {
                return Color.FromArgb(132, 253, 115);
            }
            else if (VARIvalue >= scaleValues[4] && VARIvalue < scaleValues[5])
            {
                return Color.FromArgb(215, 254, 175);
            }
            else if (VARIvalue >= scaleValues[3] && VARIvalue < scaleValues[4])
            {
                return Color.FromArgb(242, 254, 194);
            }
            else if (VARIvalue >= scaleValues[2] && VARIvalue < scaleValues[3])
            {
                return Color.PeachPuff;
            }
            else if (VARIvalue >= scaleValues[1] && VARIvalue < scaleValues[2])
            {
                return Color.DarkSalmon;
            }
            else if (VARIvalue >= scaleValues[0] && VARIvalue < scaleValues[1])
            {
                return Color.Red;
            }
            else if (VARIvalue < scaleValues[0])
            {
                return Color.Firebrick;
            }
            else
            {
                return Color.Black;
            }
        }

        public static void UseVARI(byte[] dataByteArray, double[] scaleValues)
        {
            for (int i = 0; i < dataByteArray.Length; i += 4)
            {
                //  dataByteArray[i+2] = red
                // dataByteArray[i+1] = green
                // dataByteArray[i + 3] = alpha
                Color color = Color.FromArgb(dataByteArray[i + 3], dataByteArray[i + 2], dataByteArray[i + 1], dataByteArray[i]);
                // Console.Write("color.G: " + color.G + " color.B: " + color.B + " color.R:" + color.R);
                float VARI = Convert(color);
                Color newColor = ToColorMapper(VARI, scaleValues);
                // Console.WriteLine("VARI = " + VARI + " newColor: " + newColor.Name);
                dataByteArray[i + 3] = (byte)newColor.A;
                dataByteArray[i + 2] = (byte)newColor.R;
                dataByteArray[i + 1] = (byte)newColor.G;
                dataByteArray[i] = (byte)newColor.B;
            }
        }

    }
}
