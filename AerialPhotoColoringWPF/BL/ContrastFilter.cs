﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerialPhotoColoringWPF.BL
{
    public class ContrastFilter
    {
        public static void UseContrastFilter(byte[] dataByteArray, int contrast)
        {
            double contrastLevel = Math.Pow((100.0 + contrast) / 100.0, 2);


            double blue = 0;
            double green = 0;
            double red = 0;


            for (int i = 0; i + 4 < dataByteArray.Length; i += 4)
            {
                blue = ((((dataByteArray[i] / 255.0) - 0.5) *
                            contrastLevel) + 0.5) * 255.0;


                green = ((((dataByteArray[i + 1] / 255.0) - 0.5) *
                            contrastLevel) + 0.5) * 255.0;


                red = ((((dataByteArray[i + 2] / 255.0) - 0.5) *
                            contrastLevel) + 0.5) * 255.0;


                if (blue > 255)
                { blue = 255; }
                else if (blue < 0)
                { blue = 0; }


                if (green > 255)
                { green = 255; }
                else if (green < 0)
                { green = 0; }


                if (red > 255)
                { red = 255; }
                else if (red < 0)
                { red = 0; }


                dataByteArray[i] = (byte)blue;
                dataByteArray[i + 1] = (byte)green;
                dataByteArray[i + 2] = (byte)red;
            }
        }
    }
}
