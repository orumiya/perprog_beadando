﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerialPhotoColoringWPF.BL
{
    public class EdgeDetectionFilter
    {
        public static object edgeLockObject = new Object();

        public static void UseEdgeDetectionFilter(byte[] dataByteArray, int width, int height, int stride)
        {
            // Create sharpening filter.
            const int filterWidth = 3;
            const int filterHeight = 3;

            double[,] filter = new double[filterWidth, filterHeight];
            filter[0, 0] = filter[0, 1] = filter[0, 2] = 1;
            filter[1, 0] = filter[1, 1] = filter[1, 2] = 0;
                filter[2, 1] = filter[2, 2] = filter[2, 0] = -1;

            double bias = 0.0;
            double factor = 1.0;

            lock(edgeLockObject)
            {

            
            Color[,] result = new Color[width, height];


            int rgb;
            // Fill the color array with the new sharpened color values.
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    double red = 0.0, green = 0.0, blue = 0.0;

                    for (int filterX = 0; filterX < filterWidth; filterX++)
                    {
                        for (int filterY = 0; filterY < filterHeight; filterY++)
                        {
                            int imageX = (x - filterWidth / 2 + filterX + width) % width;
                            int imageY = (y - filterHeight / 2 + filterY + height) % height;

                            rgb = imageY * stride + 4 * imageX;

                            red += dataByteArray[rgb + 2] * filter[filterX, filterY];
                            green += dataByteArray[rgb + 1] * filter[filterX, filterY];
                            blue += dataByteArray[rgb + 0] * filter[filterX, filterY];
                        }
                        int r = Math.Min(Math.Max((int)(factor * red + bias), 0), 255);
                        int g = Math.Min(Math.Max((int)(factor * green + bias), 0), 255);
                        int b = Math.Min(Math.Max((int)(factor * blue + bias), 0), 255);

                        result[x, y] = Color.FromArgb(r, g, b);
                    }
                }
            }

            // Update the image with the sharpened pixels.
            for (int x = 0; x < width; ++x)
            {
                for (int y = 0; y < height; ++y)
                {
                    rgb = y * stride + 4 * x;

                    dataByteArray[rgb + 2] = result[x, y].R;
                    dataByteArray[rgb + 1] = result[x, y].G;
                    dataByteArray[rgb + 0] = result[x, y].B;
                }
            }
            }

        }
    }
}
