﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AerialPhotoColoringWPF.BL
{
    public class BrightnessFilter
    {
        public static void UseBrightnessFilter (byte[] dataByteArray, int brightness)
        {
            for (int i = 0; i < dataByteArray.Length; i += 4)
            {
                for (int j = 0; j < 3; j++)
                {
                    int newValue = (int)(dataByteArray[i + j]) + brightness;
                    if (newValue > 255) newValue = 255;
                    if (newValue < 0) newValue = 0;
                    dataByteArray[i + j] = (byte)newValue;
                }
            }
        }
    }
}
