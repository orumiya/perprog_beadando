﻿namespace AerialPhotoColoringWPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public static class Utility
    {

        public static Bitmap Load(string path)
        {
            Bitmap bitmapImage = new Bitmap(path);
            return bitmapImage;
        }

        public static void Save(Bitmap bitmap, string name)
        {
            bitmap.Save(name, ImageFormat.Jpeg);
        }
    }
}
