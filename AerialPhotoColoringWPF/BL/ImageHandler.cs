﻿namespace AerialPhotoColoringWPF.BL
{
    using AerialPhotoColoringWPF.ViewModel;
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

    public enum FilterType
    {
        BRIGHTNESS,
        CONTRAST,
        VARI,
        EDGEDETECTION,
        SHARPEN
    }
    public static class ImageHandler
    {
        
        static double[] defaultScaleValues = { -0.5, -0.33, -0.1, 0.0, 0.1, 0.2, 0.33, 0.45, 0.66 };

        
        public static string ProcessImageData(Bitmap originalImage, int index, FilterViewModel filterViewModel, FilterType selectedFilter)
        {
            // string path = "C:\\Users\\user\\Documents\\mernokinfo\\perprog\\";
            // string filename = "DJI0047.jpg";
            // Bitmap originalImage = Utility.Load(path + filename);
            Bitmap cloneImage = (Bitmap)originalImage.Clone();
            // Console.WriteLine("Loading image from path: " + path + filename);
            // az eredeti file-t lockoljuk a memóriában
            BitmapData data = cloneImage.LockBits(new Rectangle(0, 0, cloneImage.Width, cloneImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            // létrehozunk egy pointert, ami a legelső pixelre mutat
            IntPtr pointerPixelZero = data.Scan0;
            // létrehozunk egy byte tömböt, amibe a képadatokat fogjuk átmásolni, ennek mérete
            // eredeti szélesség * hosszúság * 4 byte (ARGB érték miatt)
            byte[] dataByteArray = new byte[cloneImage.Width * cloneImage.Height * 4];
            // memóriabeli címből egy adott szakaszt a memóriából átmásol egy tömbbe és képes vissza is másolni
            Marshal.Copy(pointerPixelZero, dataByteArray, 0, dataByteArray.Length);

            if (selectedFilter == FilterType.VARI)
            {
                ProcessImage(dataByteArray, filterViewModel.VARIRangeStart, filterViewModel.VARIRangeEnd);
            } else
            {
                int filterValue;
                if (selectedFilter == FilterType.BRIGHTNESS)
                {
                    filterValue = filterViewModel.Brightness;
                } else
                {
                    filterValue = filterViewModel.Contrast;
                }
                ProcessImage(dataByteArray, cloneImage.Width, cloneImage.Height, data.Stride, selectedFilter, filterValue);
            }

            // visszaírtuk a memóriába a módosított byte tömböt
            Marshal.Copy(dataByteArray, 0, pointerPixelZero, dataByteArray.Length);
            // unlockoljuk a képet
            cloneImage.UnlockBits(data);
            string newFileName = "convertedImage" + index.ToString()+ "_"+ selectedFilter.ToString() 
                + DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Millisecond + ".jpeg";
            Utility.Save(cloneImage, newFileName);
            return newFileName;
        }

        public static void ProcessImage(byte[] dataByteArray, double filterValue1, double filterValue2)
        {
             double[] scaleValues = VARIFilter.SetScaleValues(defaultScaleValues, filterValue1, filterValue2);
             VARIFilter.UseVARI(dataByteArray, scaleValues);
        }

        public static void ProcessImage(byte[] dataByteArray, int width, int height, int stride, FilterType filterType, int filterValue)
        {
            // using filter on the picture
            if (filterType == FilterType.BRIGHTNESS)
            {
                BrightnessFilter.UseBrightnessFilter(dataByteArray, filterValue);

            }
            else if (filterType == FilterType.VARI)
            {
                double[] scaleValues = VARIFilter.SetScaleValues(defaultScaleValues, -0.5, 0.8);
                VARIFilter.UseVARI(dataByteArray, scaleValues);
            }
            else if (filterType == FilterType.CONTRAST)
            {
                ContrastFilter.UseContrastFilter(dataByteArray, filterValue);
            }
            else if (filterType == FilterType.SHARPEN)
            {
                SharpenFilter.UseSharpenFilter(dataByteArray, width, height, stride);
            }
            else if (filterType == FilterType.EDGEDETECTION)
            {
                EdgeDetectionFilter.UseEdgeDetectionFilter(dataByteArray, width, height, stride);
            }

        }


    }
}
